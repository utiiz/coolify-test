# Run locally - dev
```bash
make db_run password=<password>
make db_start
air
```

# Run locally on Docker
Create a `.env` file with the following content :
```bash
DB_NAME=<db_name>
DB_USER=<db_user>
DB_PASS=<db_pass>
DB_HOST=coolify-postgres
JWT_SECRET=<jwt_secret>
```
Network coolify-network should be created in docker-compose.yml

```bash
make build
make up
```

# TODO
- [x] Add grafana
- [x] Add prometheus
- [x] Add loki and promtail
- [ ] Add migrations
