package main

import (
	"encoding/json"
	"os"
	"utiiz/coolify-test/internal/database"
	"utiiz/coolify-test/internal/handlers"
	"utiiz/coolify-test/internal/log"
	"utiiz/coolify-test/internal/metrics"
	"utiiz/coolify-test/internal/utils"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func init() {
	utils.InitEnv()
	metrics.InitMetrics(prometheus.NewRegistry())
	database.Init()
	log.SetLevel(log.ErrorLevel)
}

func main() {
	// ------------------------------------------------
	// --                  HTTP                      --
	// ------------------------------------------------

	e := echo.New()
	e.Use(middleware.Logger())
	e.Renderer = handlers.NewTemplate()

	e.Static("/css", "internal/static/css")
	e.Static("/js", "internal/static/js")
	e.Static("/assets", "internal/static/assets")

	createAccount := e.Group("/create-account")
	createAccount.POST("/organization", handlers.CreateOrganization, handlers.CorrelationID, handlers.Authorizer)
	createAccount.GET("/organization", handlers.CreateOrganization, handlers.CorrelationID, handlers.Authorizer)

	e.POST("/login", handlers.Login, handlers.CorrelationID)
	e.POST("/sign_up", handlers.SignUp, handlers.CorrelationID)
	e.GET("/login", handlers.Login, handlers.CorrelationID)
	e.GET("/sign_up", handlers.SignUp, handlers.CorrelationID)

	e.GET("/week_load", handlers.WeekLoad, handlers.CorrelationID, handlers.Authorizer)

	e.GET("/", handlers.Home, handlers.CorrelationID, handlers.Authorizer)

	data, err := json.MarshalIndent(e.Routes(), "", "  ")
	if err != nil {
		log.Error("Failed to marshal routes", "error", err.Error())
	}
	os.WriteFile("routes.json", data, 0644)

	go func() { e.Logger.Fatal(e.Start(":1337")) }()

	// ------------------------------------------------
	// --               PROMETHEUS                   --
	// ------------------------------------------------
	ePrometheus := echo.New()

	ePrometheus.GET("/metrics", echo.WrapHandler(promhttp.HandlerFor(
		metrics.Reg,
		promhttp.HandlerOpts{},
	)))

	go func() { ePrometheus.Logger.Fatal(ePrometheus.Start(":42069")) }()

	select {}
}
