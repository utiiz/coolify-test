# Build
FROM golang:1.22 AS builder

WORKDIR /app

COPY go.mod ./

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o main .

# Use distroless as minimal base image to package the app
FROM alpine:latest

RUN apk --no-cache add ca-certificates

WORKDIR /root/

COPY --from=builder /app/main .

COPY --from=builder /app/views ./views

RUN mkdir /root/logs

RUN touch /root/logs/coolify.log

EXPOSE 1337

CMD ./main
