air: 
	air -c air.toml
build:
	docker-compose build

up:
	docker-compose up -d

db-run:
	docker run --name coolify-postgres --network coolify-network -p 5432:5432 -e POSTGRES_USER=tuteaz -e POSTGRES_PASSWORD=$(password) -e POSTGRES_DB="coolify" -d postgres:latest

db-start:
	docker container start coolify-postgres

css:
	bun run tailwindcss --config tailwind.config.js -i internal/static/css/input.css -o internal/static/css/styles.css

css-watch:
	bun run tailwindcss --config tailwind.config.js -i internal/static/css/input.css -o internal/static/css/styles.css --watch
