// tailwind.config.js
module.exports = {
    mode: 'jit',
    content: [
        './**/*.templ',
        './**/*.{html,js}',
    ],
    theme: {
        extend: {
            colors: {
                blue: {
                    100: '#E8F5FF',
                    200: '#D9EDFF',
                    300: '#98B1E0',
                    400: '#799EFE',
                    500: '#3C61C6',
                    600: '#2154F1',
                    700: '#2154F1',
                    800: '#2154F1',
                    900: '#2154F1',
                },
                red: {
                    100: '#FFF3F1',
                    200: '#FFE0DE77',
                    300: '#DA9DA2',
                    400: '#FE7979',
                    500: '#AA3541',
                    600: '#FF0024',
                    700: '#FF0024',
                    800: '#FF0024',
                    900: '#FF0024',
                }
            },
        },
    },
    plugins: [],
}
