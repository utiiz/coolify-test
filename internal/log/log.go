package log

import (
	"fmt"
	"log/slog"
	"os"
	"path/filepath"
)

const (
	DebugLevel = iota
	InfoLevel
	WarnLevel
	ErrorLevel
)

type Logger struct {
	Level int
	Log   *slog.Logger
}

var logger *Logger
var file *os.File

func init() {
	path, err := filepath.Abs("./logs")
	if err != nil {
		fmt.Println("Error getting logs path:", err)
	}

	file, err = os.OpenFile(path+"/coolify.log", os.O_CREATE|os.O_RDWR|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println("Error opening log file:", err)
		os.Exit(1)
	}

	logger = &Logger{
		Level: InfoLevel,
		Log:   slog.New(slog.NewTextHandler(file, nil)),
	}
}

func SetLevel(level int) {
	logger.Level = level
}

func Debug(msg string, args ...any) {
	if logger.Level < DebugLevel {
		return
	}
	extraArgs := []interface{}{}
	args = append(args, extraArgs...)
	logger.Log.Debug(msg, args...)
}

func Info(msg string, args ...any) {
	if logger.Level < InfoLevel {
		return
	}
	extraArgs := []interface{}{}
	args = append(args, extraArgs...)
	logger.Log.Info(msg, args...)
}

func Warn(msg string, args ...any) {
	if logger.Level < WarnLevel {
		return
	}
	extraArgs := []interface{}{}
	args = append(args, extraArgs...)
	logger.Log.Warn(msg, args...)
}

func Error(msg string, args ...any) {
	if logger.Level < ErrorLevel {
		return
	}
	extraArgs := []interface{}{}
	args = append(args, extraArgs...)
	logger.Log.Error(msg, args...)
}
