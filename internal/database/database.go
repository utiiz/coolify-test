package database

import (
	"fmt"
	"time"
	"utiiz/coolify-test/internal/log"
	"utiiz/coolify-test/internal/models"
	"utiiz/coolify-test/internal/utils"

	_ "github.com/lib/pq"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const (
	DRIVER_NAME = "postgres"
	PORT        = 5432
)

var db *gorm.DB

func Init() {
	var err error

	url := fmt.Sprintf(`%s://%s:%s@%s:%d/%s?sslmode=disable`, DRIVER_NAME, utils.ENV.DB_USER, utils.ENV.DB_PASS, utils.ENV.DB_HOST, PORT, utils.ENV.DB_NAME)
	db, err = gorm.Open(postgres.Open(url), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	db.AutoMigrate(&models.User{}, &models.Organization{}, &models.Shift{})
}

func GetUsers() ([]models.User, error) {
	var users []models.User
	result := db.Preload("Organization").Find(&users)
	if result.Error != nil {
		return nil, result.Error
	}

	return users, nil
}

func GetUser(username string) (*models.User, error) {
	user := models.User{}

	result := db.Where("username = ?", username).Preload("Organization").First(&user)
	if result.Error != nil {
		return nil, result.Error
	}

	return &user, nil
}

func GetUserById(id int) (*models.User, error) {
	user := &models.User{}

	result := db.Where("id = ?", id).Preload("Organization").Preload("Shifts").First(user)
	log.Info("GetUserById", "user", user)
	if result.Error != nil {
		return nil, result.Error
	}

	return user, nil
}

func GetUsersInOrganization(id *uint) ([]models.User, error) {
	var users []models.User
	result := db.Where("organization_id = ?", id).Find(&users)
	if result.Error != nil {
		return nil, result.Error
	}
	return users, nil
}

func GetWeeklyShiftsForUser(id *uint, start time.Time, end time.Time) ([]*models.Shift, error) {
	var shifts []*models.Shift
	result := db.Where("user_id = ? AND start_time >= ? AND end_time <= ?", id, start, end).Find(&shifts)
	if result.Error != nil {
		return nil, result.Error
	}
	return shifts, nil
}

func CreateUser(user *models.User) error {
	result := db.Create(&user)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func UpdateUser(user *models.User) error {
	result := db.Save(&user)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func CreateOrganization(organization *models.Organization) error {
	result := db.Create(&organization)
	if result.Error != nil {
		return result.Error
	}
	return nil
}
