package models

import (
	"time"

	"gorm.io/gorm"
)

type Shift struct {
	gorm.Model
	StartTime time.Time
	EndTime   time.Time
	Absence   bool `gorm:"default:false"`
	UserID    uint
	User      *User `gorm:"foreignKey:UserID;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}
