package models

import (
	"database/sql"

	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Username       string
	Password       string
	Lastname       sql.NullString
	Firstname      sql.NullString
	Email          sql.NullString
	OrganizationID *uint
	Organization   *Organization `gorm:"foreignKey:OrganizationID;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	Shifts         []*Shift
}
