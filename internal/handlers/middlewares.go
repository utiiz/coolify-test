package handlers

import (
	"fmt"
	"net/http"
	"time"
	"utiiz/coolify-test/internal/database"
	"utiiz/coolify-test/internal/log"
	"utiiz/coolify-test/internal/utils"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
)

const HEADER_X_CORRELATION_ID = "X-Correlation-ID"

func Authorizer(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		fmt.Printf("Authorizer\n")
		// Get the cookie from the request
		tokenString, err := c.Cookie(utils.COOKIE_AUTH)
		fmt.Printf("tokenString: %v\n", tokenString)
		fmt.Printf("err: %v\n", err)
		if err != nil {
			log.Warn("Failed to get cookie", "error", err.Error())
			return c.Redirect(http.StatusFound, "/login")
		}

		// Decode/validate the cookie
		token, err := jwt.Parse(tokenString.Value, func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
			}
			return []byte(utils.ENV.JWT_SECRET), nil
		})

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			// Check the expiration
			if float64(time.Now().In(time.UTC).Unix()) > claims["exp"].(float64) {
				return c.Redirect(http.StatusFound, "/login")
			}

			// Find the user with token
			id := int(claims["sub"].(float64))
			user, err := database.GetUserById(id)
			if err != nil {
				return c.Redirect(http.StatusFound, "/login")
			}
			fmt.Printf("User: %+v\n", user)

			// Attach to the request
			c.Set("user", user)

			// Continue
			return next(c)
		} else {
			return c.Redirect(http.StatusFound, "/login")
		}
	}

}

func CorrelationID(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		fmt.Println("CorrelationID")
		c.Response().Header().Set(HEADER_X_CORRELATION_ID, "coolify-app")
		return next(c)
	}
}
