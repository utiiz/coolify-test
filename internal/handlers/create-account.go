package handlers

import (
	"net/http"
	"utiiz/coolify-test/internal/database"
	"utiiz/coolify-test/internal/log"
	"utiiz/coolify-test/internal/models"
	"utiiz/coolify-test/internal/utils"

	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
)

func SignUp(c echo.Context) error {
	// ------------------------------------------------
	// --                   GET                      --
	// ------------------------------------------------
	if c.Request().Method == http.MethodGet {
		return c.Render(http.StatusOK, "sign_up", nil)
	}

	// ------------------------------------------------
	// --                   POST                     --
	// ------------------------------------------------
	var body struct {
		Username string `form:"username" json:"username"`
		Password string `form:"password" json:"password"`
	}

	err := c.Bind(&body)
	if err != nil {
		log.Error("Failed to bind", "error", err.Error())
		return c.String(http.StatusBadRequest, err.Error())
	}

	// Check if the user exists
	_, err = database.GetUser(body.Username)
	if err == nil {
		log.Error("User already exists", "username", body.Username)
		return c.String(http.StatusBadRequest, "User already exists")
	}

	// Create the user with hashed password in the database
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(body.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Error("Failed to hash password", "error", err.Error())
		return c.String(http.StatusInternalServerError, "Failed to hash password")
	}

	user := models.User{
		Username: body.Username,
		Password: string(hashedPassword),
	}

	err = database.CreateUser(&user)
	if err != nil {
		log.Error("Failed to create user", "error", err.Error())
		return c.String(http.StatusInternalServerError, "Failed to create user")
	}

	log.Info("Account created", "user", user)

	// Set the cookie
	err = utils.SetAuthCookie(&user, c)
	if err != nil {
		log.Error("Failed to set auth cookie", "error", err.Error())
		return c.String(http.StatusInternalServerError, "Failed to set auth cookie")
	}

	// Redirect to /
	c.Response().Header().Set("HX-Redirect", "/create-account/organization")
	return c.NoContent(http.StatusNoContent)
}

func CreateOrganization(c echo.Context) error {
	log.Info("Creating organization")
	// ------------------------------------------------
	// --                   GET                      --
	// ------------------------------------------------
	if c.Request().Method == http.MethodGet {
		return c.Render(http.StatusOK, "create_account_organization", nil)
	}

	// ------------------------------------------------
	// --                   POST                     --
	// ------------------------------------------------
	var body struct {
		Name string `form:"name" json:"name"`
	}

	err := c.Bind(&body)
	if err != nil {
		log.Error("Failed to bind", "error", err.Error())
		return c.String(http.StatusBadRequest, err.Error())
	}

	org := &models.Organization{
		Name: body.Name,
	}

	err = database.CreateOrganization(org)
	if err != nil {
		log.Error("Failed to create organization", "error", err.Error())
		return c.String(http.StatusInternalServerError, "Failed to create organization")
	}

	log.Info("Organization created", "organization", org)

	// Update the user with the organization
	user := c.Get("user").(*models.User)
	user.Organization = org

	err = database.UpdateUser(user)

	log.Info("Updated user", "user", user)

	// Redirect to /
	c.Response().Header().Set("HX-Redirect", "/")
	return c.NoContent(http.StatusNoContent)
}
