package handlers

import (
	"fmt"
	"io"
	"math"
	"net/http"
	"sort"
	"strconv"
	"time"

	"text/template"
	"utiiz/coolify-test/internal/database"
	"utiiz/coolify-test/internal/log"
	"utiiz/coolify-test/internal/metrics"
	"utiiz/coolify-test/internal/models"
	"utiiz/coolify-test/internal/utils"

	"github.com/labstack/echo/v4"
	"golang.org/x/crypto/bcrypt"
)

type Templates struct {
	templates *template.Template
}

func (t *Templates) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	return t.templates.ExecuteTemplate(w, name, data)
}

func NewTemplate() *Templates {
	funcMap := template.FuncMap{
		"FirstLetter": func(s string) string {
			if len(s) == 0 {
				return ""
			}
			return string(s[0])
		},
		"GetShiftsTime": func(a []*models.Shift) string {
			sort.Slice(a, func(i, j int) bool { return a[i].StartTime.UTC().Before(a[j].StartTime.UTC()) })
			first := a[0]
			sort.Slice(a, func(i, j int) bool { return a[i].EndTime.UTC().Before(a[j].EndTime.UTC()) })
			last := a[len(a)-1]
			return fmt.Sprintf("%s - %s", first.StartTime.UTC().Format("15:04"), last.EndTime.UTC().Format("15:04"))
		},
		"AreShiftsOff": func(a []*models.Shift) bool {
			areShiftsOff := true
			for _, shift := range a {
				if !shift.Absence {
					areShiftsOff = false
					break
				}
			}

			return areShiftsOff
		},
		"GetWeekSummary": func(year, week int) string {
			fmt.Printf("year: %d, week: %d\n", year, week)
			firstDayOfYear := time.Date(year, time.January, 1, 0, 0, 0, 0, time.UTC)

			firstDayOfWeek := firstDayOfYear.AddDate(0, 0, (week-1)*7-int(firstDayOfYear.Weekday())+1)
			lastDayOfWeek := firstDayOfWeek.AddDate(0, 0, 6)
			return fmt.Sprintf("%d - %d %s", firstDayOfWeek.Day(), lastDayOfWeek.Day(), firstDayOfWeek.Format("January"))
		},
		"IsDayFull": func(a []*models.Shift) bool {
			// Get the number of hours for each shifts and sum them
			sum := 0.0
			for _, shift := range a {
				sum += shift.EndTime.Sub(shift.StartTime).Hours()
			}
			return sum >= 8.0
		},
		"GetWeeklyTime": func(a [][]*models.Shift) float64 {
			sum := 0.0
			for _, shifts := range a {
				for _, shift := range shifts {
					sum += shift.EndTime.Sub(shift.StartTime).Hours()
				}
			}
			return sum
		},
		"GetWeeklyLoad": func(a [][]*models.Shift) float64 {
			sum := 0.0
			for _, shifts := range a {
				for _, shift := range shifts {
					sum += shift.EndTime.Sub(shift.StartTime).Hours()
				}
			}
			return math.Min(sum/40*100, 100)
		},
		"GetPreviousWeekURL": func(year, week int) string {
			if week == 1 {
				year--
				week = 52
			} else {
				week--
			}
			return fmt.Sprintf("/week_load?year=%d&week=%d", year, week)
		},
		"GetNextWeekURL": func(year, week int) string {
			if week == 52 {
				year++
				week = 1
			} else {
				week++
			}
			return fmt.Sprintf("/week_load?year=%d&week=%d", year, week)
		},
	}
	tmpl := template.New("").Funcs(funcMap)
	tmpl, err := tmpl.ParseGlob("internal/templates/**/*.html")
	if err != nil {
		log.Error("Failed to parse templates", "error", err.Error())
	}
	return &Templates{templates: tmpl}
}

func Home(c echo.Context) error {
	type Data struct {
		User         models.User
		Organization models.Organization
		Year         int
		Week         int
		Headers      []Header
		Rows         []Rows
	}

	// Get the week number
	year, week := time.Now().In(time.UTC).ISOWeek()

	user := c.Get("user").(*models.User)
	users, err := database.GetUsersInOrganization(user.OrganizationID)
	if err != nil {
		log.Error("Failed to get users", "error", err.Error())
	}

	var updatedUsers []models.User
	updatedUsers = append(updatedUsers, *user)
	for _, u := range users {
		if user.ID != u.ID {
			updatedUsers = append(updatedUsers, u)
		}
	}

	firstDayOfWeek := time.Now().In(time.UTC).AddDate(0, 0, -(int(time.Now().Weekday()) - 1)).Truncate(24 * time.Hour)
	lastDayOfWeek := firstDayOfWeek.AddDate(0, 0, 6).Add(24*time.Hour - time.Nanosecond)

	data := Data{
		User:         *user,
		Organization: *user.Organization,
		Year:         year,
		Week:         week,
		Headers:      GetHeaders(time.Now().In(time.UTC)),
		Rows:         GetRows(updatedUsers, firstDayOfWeek, lastDayOfWeek),
	}
	metrics.M.Visit.Inc()
	return c.Render(http.StatusOK, "index", data)
}

func WeekLoad(c echo.Context) error {
	type Data struct {
		User         models.User
		Organization models.Organization
		Year         int
		Week         int
		Headers      []Header
		Rows         []Rows
	}
	// Retrieve the year and week from the URL parameters
	year, err := strconv.Atoi(c.QueryParam("year"))
	if err != nil {
		log.Error("Failed to parse year", "error", err.Error())
		return c.String(http.StatusBadRequest, err.Error())
	}
	week, err := strconv.Atoi(c.QueryParam("week"))
	if err != nil {
		log.Error("Failed to parse week", "error", err.Error())
		return c.String(http.StatusBadRequest, err.Error())
	}

	// Get the user
	user := c.Get("user").(*models.User)
	users, err := database.GetUsersInOrganization(user.OrganizationID)
	if err != nil {
		log.Error("Failed to get users", "error", err.Error())
	}

	var updatedUsers []models.User
	updatedUsers = append(updatedUsers, *user)
	for _, u := range users {
		if user.ID != u.ID {
			updatedUsers = append(updatedUsers, u)
		}
	}

	// Get the week first day
	firstDayOfYear := time.Date(year, time.January, 1, 0, 0, 0, 0, time.UTC)
	firstDayOfWeek := firstDayOfYear.AddDate(0, 0, (week-1)*7-int(firstDayOfYear.Weekday())+1).Truncate(24 * time.Hour)
	lastDayOfWeek := firstDayOfWeek.AddDate(0, 0, 6).Add(24*time.Hour - time.Nanosecond)

	data := Data{
		User:         *user,
		Organization: *user.Organization,
		Year:         year,
		Week:         week,
		Headers:      GetHeaders(firstDayOfWeek),
		Rows:         GetRows(updatedUsers, firstDayOfWeek, lastDayOfWeek),
	}
	metrics.M.Visit.Inc()
	return c.Render(http.StatusOK, "weekly_shifts", data)
}

// curl -X POST -H "Content-Type: application/json" -d '{"username": "admin", "password": "admin"}' http://localhost:1337/login
func Login(c echo.Context) error {
	// ------------------------------------------------
	// --                   GET                      --
	// ------------------------------------------------
	if c.Request().Method == http.MethodGet {
		users, err := database.GetUsers()
		fmt.Printf("users: %v\n", users)
		fmt.Printf("err: %v\n", err)
		return c.Render(http.StatusOK, "login", nil)
	}

	// ------------------------------------------------
	// --                   POST                     --
	// ------------------------------------------------
	var body struct {
		Username string `form:"username" json:"username"`
		Password string `form:"password" json:"password"`
	}

	err := c.Bind(&body)
	if err != nil {
		log.Error("Failed to bind", "error", err.Error())
		return c.String(http.StatusBadRequest, err.Error())
	}

	// Look up the requested user
	user, err := database.GetUser(body.Username)
	fmt.Printf("user: %v\n", user)
	if err != nil {
		log.Error("Failed to get user", "error", err.Error())
		return c.String(http.StatusNotFound, "Username or password is incorrect")
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(body.Password))
	if err != nil {
		log.Error("Failed to compare hash", "error", err.Error())
		return c.String(http.StatusNotFound, "Username or password is incorrect")
	}

	err = utils.SetAuthCookie(user, c)
	if err != nil {
		log.Error("Failed to create token", "error", err.Error())
		return c.String(http.StatusInternalServerError, "Failed to create token")
	}

	log.Info("Logged in", "user", user.Username)

	// Redirect to /
	c.Response().Header().Set("HX-Redirect", "/")
	return c.NoContent(http.StatusNoContent)
}

func GetWeekDays(day time.Time) []time.Time {
	offset := int(time.Monday - day.Weekday())
	if offset > 0 {
		offset -= 6
	}

	weekStart := day.AddDate(0, 0, offset)
	var week []time.Time
	for i := 0; i < 7; i++ {
		week = append(week, weekStart.AddDate(0, 0, i))
	}
	return week
}

type Header struct {
	WeekDay    string
	WeekNumber int
	IsToday    bool
}

func GetHeaders(day time.Time) []Header {
	var headers []Header

	for _, d := range GetWeekDays(day) {
		headers = append(headers, Header{
			WeekDay:    d.Format("Monday")[0:2],
			WeekNumber: d.Day(),
			IsToday:    isToday(d),
		})
	}

	return headers
}

type Rows struct {
	User   models.User
	Shifts [][]*models.Shift
}

func GetRows(users []models.User, start time.Time, end time.Time) []Rows {
	rows := []Rows{}
	for _, user := range users {
		// Get shifts from database
		userShifts, err := database.GetWeeklyShiftsForUser(&user.ID, start, end)
		if err != nil {
			log.Error("Failed to get shifts", "error", err.Error())
		}

		shifts := make([][]*models.Shift, 7)
		for _, shift := range userShifts {
			idx := (shift.StartTime.UTC().Weekday() + 6) % 7
			shifts[idx] = append(shifts[idx], shift)
		}

		rows = append(rows, Rows{
			User:   user,
			Shifts: shifts,
		})
	}
	return rows
}

func isToday(t time.Time) bool {
	now := time.Now().In(time.UTC)
	today := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
	checkDate := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location())
	return today.Equal(checkDate)
}
