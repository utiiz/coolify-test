package metrics

import "github.com/prometheus/client_golang/prometheus"

type Metrics struct {
	Visit prometheus.Counter
}

var M *Metrics
var Reg *prometheus.Registry

func InitMetrics(reg *prometheus.Registry) {
	Reg = reg
	M = &Metrics{
		Visit: prometheus.NewCounter(
			prometheus.CounterOpts{
				Namespace: "coolify_test",
				Name:      "visits",
				Help:      "Number of visits",
			},
		),
	}
	reg.MustRegister(M.Visit)
}
