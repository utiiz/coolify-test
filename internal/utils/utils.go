package utils

import (
	"net/http"
	"os"
	"time"
	"utiiz/coolify-test/internal/models"

	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
)

const COOKIE_AUTH = "Authorization"

type Env struct {
	DB_HOST    string
	DB_USER    string
	DB_PASS    string
	DB_NAME    string
	JWT_SECRET string
}

var ENV *Env

func InitEnv() error {
	host := os.Getenv("DB_HOST")
	user := os.Getenv("DB_USER")
	pass := os.Getenv("DB_PASS")
	name := os.Getenv("DB_NAME")
	jwtSecret := os.Getenv("JWT_SECRET")

	ENV = &Env{
		DB_HOST:    host,
		DB_USER:    user,
		DB_PASS:    pass,
		DB_NAME:    name,
		JWT_SECRET: jwtSecret,
	}
	return nil
}

func SetAuthCookie(user *models.User, c echo.Context) error {
	// Create a new token object, specifying signing method and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub": user.ID,
		"exp": time.Now().In(time.UTC).Add(time.Hour * 72).Unix(),
	})

	tokenString, err := token.SignedString([]byte(ENV.JWT_SECRET))
	if err != nil {
		return err
	}

	// Set cookies
	c.SetCookie(&http.Cookie{
		Name:     COOKIE_AUTH,
		Value:    tokenString,
		Expires:  time.Now().In(time.UTC).Add(time.Hour * 72),
		HttpOnly: true,
		SameSite: http.SameSiteLaxMode,
	})

	return nil
}
